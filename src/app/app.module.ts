import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// App Modules
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';

// Components
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    BrowserAnimationsModule,
    SharedModule.forRoot(), // importing SharedModule with the providers attached
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
