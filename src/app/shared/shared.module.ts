import { ModuleWithProviders, NgModule } from '@angular/core';

// Components 

// Modules
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module'; 

/**
 * Module to aggregate needed components and modules
 * across the app from third party vendors
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule, 
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule, 
  ],
  /* no providers here, use providers array inside forRoot() method below */
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        /* all shared services here */
      ],
    };
  }
}
