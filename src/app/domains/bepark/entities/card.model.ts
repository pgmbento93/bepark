export interface Card {
  id: number;
  days: number; 
  favourite: boolean; 
}
