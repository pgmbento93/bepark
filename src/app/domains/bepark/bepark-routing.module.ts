import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components 
import { BeparkComponent, PlanningPeriodComponent } from './views';

// Resolvers 

const routes: Routes = [
  {
    path: '',
    component: BeparkComponent,
      children: [
        {
          path: 'planning-period',
          component: PlanningPeriodComponent,
          // resolve: { response: Resolver },
        },
       
     ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeParkRoutingModule { }
