import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbar } from 'src/app/core/containers/layout/entities';

// Entities
import { Card } from '../../entities';

@Component({
  selector: 'app-planning-period',
  templateUrl: './planning-period.component.html',
  styleUrls: ['./planning-period.component.scss']
})
export class PlanningPeriodComponent implements OnInit {
  /**
     * Snackbar
     */
  private readonly snackbar: CustomSnackbar;

  /**
   * Card list of planning period component
   */
  public cardList: Card[] = [{ id: 1, days: 1, favourite: true }, { id: 2, days: 10, favourite: false }, { id: 3, days: 25, favourite: false }, { id: 4, days: 100, favourite: false }]

  /**
   * Add new card of planning period component
   */
  public addNewCard = false

  /**
   * Creates an instance of planning period component.
   * @param matSnackBar 
   */
  constructor(private readonly matSnackBar: MatSnackBar) {
    this.snackbar = new CustomSnackbar(this.matSnackBar);
  }

  ngOnInit(): void {
  }

  private sortCardList(): Card[] {
    return this.cardList.sort((a, b) => (a.days > b.days) ? 1 : -1)
  }

  /**
   * Cards change
   * @param card 
   */
  public cardChange(card: Card) {
    let updateItem = this.cardList.find(this.findIndexToUpdate, card.id);

    let index = this.cardList.indexOf(updateItem);

    const getFavouriteCard = this.cardList.find(card => card.favourite)

    if (getFavouriteCard) { getFavouriteCard.favourite = false }

    this.cardList[index] = card;

    this.sortCardList()
    this.snackbar.showSnackbar('success', 'Changes were successfully update');
  }

  /**
   * Deletes card
   * @param card 
   */
  public deleteCard(id: number) {
    this.cardList = this.cardList.filter(item => item.id !== id);
    this.snackbar.showSnackbar('success', 'Changes were successfully update');
  }

  /**
   * Finds index to update
   * @param newItem 
   * @returns  
   */
  private findIndexToUpdate(newItem) {
    return newItem.id === this;
  }

  /**
   * Opens new card
   */
  public openNewCard() {
    this.addNewCard = true
  }

  /**
   * Adds card
   * @param card 
   */
  public addCard(card: Card) {
    if (card.days) {
      this.cardList = [...this.cardList, card]
      this.sortCardList()
    }
    this.addNewCard = false
    this.snackbar.showSnackbar('success', 'Changes were successfully update');
  }
}
