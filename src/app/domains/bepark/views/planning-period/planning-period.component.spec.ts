import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningPeriodComponent } from './planning-period.component';

describe('PlanningPeriodComponent', () => {
  let component: PlanningPeriodComponent;
  let fixture: ComponentFixture<PlanningPeriodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanningPeriodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
