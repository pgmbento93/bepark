import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeparkComponent } from './bepark.component';

describe('BeparkComponent', () => {
  let component: BeparkComponent;
  let fixture: ComponentFixture<BeparkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeparkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeparkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
