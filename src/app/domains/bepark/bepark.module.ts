import { NgModule } from '@angular/core';
import { LayoutModule } from 'src/app/core/containers/layout/layout.module';

// App Modules 
import { SharedModule } from 'src/app/shared/shared.module';
import { BeParkRoutingModule } from './bepark-routing.module';

// Components 
import { BeparkComponent, PlanningPeriodComponent } from './views';


@NgModule({
  declarations: [
    BeparkComponent,
    PlanningPeriodComponent,
  ],
  imports: [BeParkRoutingModule, SharedModule,LayoutModule],
})
export class BeParkModule { }
