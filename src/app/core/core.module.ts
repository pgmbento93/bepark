import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
 
// Modules
import { LayoutModule } from './containers/layout/layout.module';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  exports: [HttpClientModule, LayoutModule],
})
export class CoreModule {
  /**
   * Creates an instance of core module.
   *
   * @param core
   */
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
      throw new Error(
        `You should not import this module twice, it should be only imported on AppModule.
        Shared components, should be imported on SharedModule.`
      );
    }
  }
}
