import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Modules
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { BaseComponent, ToolbarComponent } from './views';
import { ButtonStepperComponent, CardDayComponent, CustomSnackbarComponent } from './views/components';

@NgModule({
  declarations: [BaseComponent, ToolbarComponent, ButtonStepperComponent, CardDayComponent, CustomSnackbarComponent],
  imports: [SharedModule, RouterModule],
  exports: [BaseComponent, ToolbarComponent, ButtonStepperComponent, CardDayComponent, CustomSnackbarComponent],
  entryComponents: [],
})
export class LayoutModule { }
