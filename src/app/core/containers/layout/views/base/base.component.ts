import { Component, OnInit } from '@angular/core';

// Services 

// Rxjs 

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {


  /**
   * Creates an instance of base component.
   */
  constructor() { }

  /**
   * OnInit
   */
  public ngOnInit() {
  }

}
