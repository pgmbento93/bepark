import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

  /**
   * Creates an instance of toolbar component.
   */
  constructor() { }

  /**
   * on init
   */
  ngOnInit(): void { }

}
