import { Component, Inject } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

// Entities
import { CustomSnackbarModel } from '../../../entities';
 
@Component({
  selector: 'app-custom-snackbar',
  templateUrl: './custom-snackbar.component.html',
  styleUrls: ['./custom-snackbar.component.scss'],
})
export class CustomSnackbarComponent {
  /**
   * Messages to show to the user.
   */
  public messages: string[] = [];

  /**
   * Creates an instance of CustomSnackbarComponent.
   *
   * @param snackBarRef
   * @param data
   */
  constructor(
    public snackBarRef: MatSnackBarRef<CustomSnackbarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: CustomSnackbarModel
  ) {
    // If message is a string
    if (typeof data.messages === 'string') {
      this.messages.push(data.messages);
      return;
    }

    // If we have an array with messages
    for (const message of data.messages) {
      this.messages.push(message);
    }
  }

  /**
   * Closes the dialog.
   */
  public close() {
    this.snackBarRef.dismiss();
  }
}
