import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-stepper',
  templateUrl: './button-stepper.component.html',
  styleUrls: ['./button-stepper.component.scss']
})
export class ButtonStepperComponent implements OnInit {

  @Input()
  public btnType: string

  @Input()
  public btnLabel: string

  constructor() { }

  ngOnInit(): void {
  }

}
