import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

// Entities
import { Card } from 'src/app/domains/bepark/entities';

@Component({
  selector: 'app-card-day',
  templateUrl: './card-day.component.html',
  styleUrls: ['./card-day.component.scss']
})
export class CardDayComponent implements OnInit {

  /**
   * Input  of card day component
   */
  @Input()
  public card: Card

  /**
   * Input  of card day component
   */
  @Input()
  public addNew: boolean

  /**
   * Days input of card day component
   */
  public daysInput: number

  /**
   * Output  of card day component
   */
  @Output() clickCardEvent = new EventEmitter<Card>();

  /**
   * Output  of card day component
   */
  @Output() addCardEvent = new EventEmitter<Card>();

  /**
   * Output  of card day component
   */
  @Output() deleteCardEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    if (this.addNew) {
      this.card = {
        id: this.getRandomId(),
        favourite: false,
        days: 4
      }
    }
  }

  /**
   * Gets random id
   * @returns random id 
   */
  public getRandomId(): number {
    return Date.now();
  }

  /**
   * Focus out function
   */
  public focusOutFunction() {
    this.card.days = this.daysInput
    this.addCardEvent.emit(this.card)
  }

  /**
   * Changes favourite
   */
  public changeFavourite() {
    let newCard = { ...this.card }
    newCard.favourite = !this.card.favourite
    this.clickCardEvent.emit(newCard)
  }

  /**
   * Deletes card
   */
  public deleteCard() {
    this.deleteCardEvent.emit(this.card.id)
  }
}
