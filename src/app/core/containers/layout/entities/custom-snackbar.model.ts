export interface CustomSnackbarModel {
  messages: any;
  type: {
    enum: {
      error;
      info;
      success;
    };
  };
}
