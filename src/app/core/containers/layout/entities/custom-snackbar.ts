import { MatSnackBar } from '@angular/material/snack-bar';

// Components
import { CustomSnackbarComponent } from '../views/components';

// Entities
import { CustomSnackbarModel } from './custom-snackbar.model';

export class CustomSnackbar implements CustomSnackbarModel {
  get messages(): any {
    return this.messages;
  }

  get type(): {
    enum: {
      error;
      info;
      success;
    };
  } {
    return this.type;
  }

  /**
   * Creates an instance of CustomSnackbar.
   *
   * @param snackBar
   */
  constructor(private readonly snackBar: MatSnackBar) {}

  /**
   * Show a snackbar with a message
   *
   * @param type
   * @param message
   * @param [duration]
   * @param [dismissible]
   */
  public showSnackbar(
    type: string,
    message: string | string[],
    duration?: number,
    dismissible?: boolean
  ) {
    const snackbarConfig = {
      panelClass: ['snackbar-' + type],
      data: {
        messages: message,
        type,
        dismissible,
      },
      duration: 0,
    };

    if (!dismissible) {
      snackbarConfig.duration = typeof duration !== 'undefined' ? duration : 5000;
      // snackbarConfig.duration = typeof duration !== 'undefined' ? duration : 100000000000;
    }

    switch (type) {
      case 'success':
        if (!this.snackBar._openedSnackBarRef) {
          this.snackBar.openFromComponent(CustomSnackbarComponent, snackbarConfig);
        }
        break;
      case 'warning':
      case 'error':
        this.snackBar.openFromComponent(CustomSnackbarComponent, snackbarConfig);
    }
  }
}
