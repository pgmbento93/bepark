import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'bepark',
    loadChildren: () => import('./domains/bepark/bepark.module').then((m) => m.BeParkModule),
  },
  {
    path: '',
    redirectTo: '/bepark/planning-period',
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/' },
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
